<?php
return [
    'daux_configuration' => [
        'global_config'     => __DIR__ . '/global.json',
        'daux_config'       => __DIR__ . '/config.json',
        'landing_page'      => __DIR__ . '/../docs/index.md',
        'daux_tmp_path'     => 'public/tmp',
        'global_output'     => 'public/docs',
        'build_on_request'  => false,
        'include_daux_module_docs' => true,
        'modules' => [
            'DauxModule' => [
                'root_path' => __DIR__ . '/../docs',
            ]
        ]
    ],
    'controllers' => [
        'invokables' => [
            'daux_console_controller' => 'DauxModule\Controller\DauxConsoleController',
            'daux_controller'         => 'DauxModule\Controller\DauxController',
        ]
    ],
    'service_manager'=>[
        'factories' => [
            'daux.service' => 'DauxModule\Service\Factory\DauxServiceFactory'
        ]
    ],
    'router' => [
        'routes' => [
            'docs' => [
                'type' => 'regex',
                'options' => [
                    'regex'    => '/docs/(?<page>[a-zA-Z0-9_-]+)(\.(?<format>(html)))?',
                    'defaults' => [
                        'controller' => 'daux_controller',
                        'action'     => 'index',
                        'format'     => 'html',
                    ],
                    'spec' => '/docs/%page%.%format%'
                ],
            ]
        ]
    ],
    'console' => [
        'router' => [
            'routes' => [
                'build' => [
                    'options' => [
                        'route'    => 'daux build',
                        'defaults' => [
                            'controller' => 'daux_console_controller',
                            'action'     => 'build'
                        ]
                    ]
                ],
                'setup' => [
                    'options' => [
                        'route'    => 'daux setup',
                        'defaults' => [
                            'controller' => 'daux_console_controller',
                            'action'     => 'setup'
                        ]
                    ]
                ],
            ]
        ]
    ]
];