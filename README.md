### The ZendFramework 2 module for daux.io documentation generator
With DauxModule you can document your zf2 modules and generate very good looking documentation.

## Installation

1. Install package by composer. Don't know how? [Take a look here](http://getcomposer.org/doc/00-intro.md#introduction)
``` json
{"require": {
	"stefanorg/daux-module": "dev-master"
}}
```

2. Create docs directory in public folder.
```
php public/index.php daux setup
```

3. Setup your docs configuration in your `module.config.php`.
```php
'daux_configuration' => [
	'modules'=>[
		'MyModule' => [
			'root_path' => __DIR__ . '/../docs',
		]
	]
]
```
