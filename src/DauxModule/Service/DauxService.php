<?php

namespace DauxModule\Service;

use DauxModule\Utils\FileUtils;
use Todaymade\Daux\Daux;
use Todaymade\Daux\ErrorPage;

class DauxService {

    protected $config;
    protected $dauxLib;
    protected $tmpPath;


    function __construct(DauxOptions $config)
    {
        $this->config = $config;
        $this->dauxLib = new Daux($config->getGlobalConfig());
    }

    /**
     * @return DauxOptions
     */
    public function getConfig(){
        return $this->config;
    }

    protected function  getModules(){
        $modules = $this->config->getModules();
        if(isset($modules['DauxModule']) && !$this->config->getIncludeDauxModuleDocs()){
            unset($modules['DauxModule']);
        }
        return $modules;
    }

    public function build()
    {
        $tmpPath = getcwd() . DIRECTORY_SEPARATOR . $this->config->getDauxTmpPath();

        //clean tmp directory
        if(!is_dir($tmpPath)){
            @mkdir($tmpPath);
        }else{
            FileUtils::clean_directory($tmpPath);
        }

        foreach($this->getModules() as $moduleName => $module){
            $path = $module['root_path'];
            $dest = $tmpPath . DIRECTORY_SEPARATOR . $moduleName;
            FileUtils::copy_recursive($path,  $dest);
        }

        //copy config file
        copy($this->config->getDauxConfig(), $tmpPath . DIRECTORY_SEPARATOR . 'config.json');
        //copy landing page
        copy($this->config->getLandingPage(), $tmpPath . DIRECTORY_SEPARATOR . basename($this->config->getLandingPage()));

        //generate daux
        $lib = new Daux($this->config->getGlobalConfig());
        $lib->setDocsPath($tmpPath);
        $lib->initialize();
        if($lib->hasError()){
            $this->handleErrorPage($lib->getErrorPage());
        }
        $outpath = getcwd() . DIRECTORY_SEPARATOR .$this->config->getGlobalOutput();
        $lib->generate_static($outpath);
    }

    public function handleErrorPage(ErrorPage $errorPage){
        //TODO: better error handling
        throw new \Exception(sprintf("Error during daux generation: %s", print_r($errorPage, true)));
    }
}