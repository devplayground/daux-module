<?php

namespace DauxModule\Service;


use Zend\Stdlib\AbstractOptions;

class DauxOptions extends AbstractOptions
{

    private $globalConfig;
    private $dauxConfig;
    private $landingPage;
    private $dauxTmpPath;
    private $globalOutput;
    private $buildOnRequest;
    private $includeDauxModuleDocs;
    private $modules = array();

    /**
     * @return mixed
     */
    public function getGlobalConfig()
    {
        return $this->globalConfig;
    }

    /**
     * @param mixed $globalConfig
     *
     * @return self
     */
    public function setGlobalConfig($globalConfig)
    {
        $this->globalConfig = $globalConfig;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDauxConfig()
    {
        return $this->dauxConfig;
    }

    /**
     * @param mixed $dauxConfig
     *
     * @return self
     */
    public function setDauxConfig($dauxConfig)
    {
        $this->dauxConfig = $dauxConfig;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLandingPage()
    {
        return $this->landingPage;
    }

    /**
     * @param mixed $landingPage
     *
     * @return self
     */
    public function setLandingPage($landingPage)
    {
        $this->landingPage = $landingPage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDauxTmpPath()
    {
        return $this->dauxTmpPath;
    }

    /**
     * @param mixed $dauxTmpPath
     *
     * @return self
     */
    public function setDauxTmpPath($dauxTmpPath)
    {
        $this->dauxTmpPath = $dauxTmpPath;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGlobalOutput()
    {
        return $this->globalOutput;
    }

    /**
     * @param mixed $globalOutput
     *
     * @return self
     */
    public function setGlobalOutput($globalOutput)
    {
        $this->globalOutput = $globalOutput;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBuildOnRequest()
    {
        return $this->buildOnRequest;
    }

    /**
     * @param mixed $buildOnRequest
     *
     * @return self
     */
    public function setBuildOnRequest($buildOnRequest)
    {
        $this->buildOnRequest = $buildOnRequest;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIncludeDauxModuleDocs()
    {
        return $this->includeDauxModuleDocs;
    }

    /**
     * @param mixed $includeDauxModuleDocs
     *
     * @return self
     */
    public function setIncludeDauxModuleDocs($includeDauxModuleDocs)
    {
        $this->includeDauxModuleDocs = $includeDauxModuleDocs;
        return $this;
    }

    /**
     * @return array
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @param array $modules
     *
     * @return self
     */
    public function setModules($modules)
    {
        $this->modules = $modules;
        return $this;
    }
}