<?php

namespace DauxModule\Service\Factory;

use DauxModule\Service\DauxOptions;
use DauxModule\Service\DauxService;
use Todaymade\Daux\Daux;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DauxServiceFactory implements FactoryInterface{

    public function createService(ServiceLocatorInterface $sl)
    {
        $config = $sl->get('config');

        if(!isset($config['daux_configuration'])){
            throw new \RuntimeException('No daux configuration found. Please set daux_configuration in your config.');
        }

        $dauxConfig = new DauxOptions($config['daux_configuration']);

        $service = new DauxService($dauxConfig);

        return $service;
    }

}