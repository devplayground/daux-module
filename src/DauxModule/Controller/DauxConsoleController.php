<?php

namespace DauxModule\Controller;

use DauxModule\Service\DauxService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DauxConsoleController extends AbstractActionController
{

    public function buildAction()
    {
        /** @var DauxService $daux */
        $daux = $this->getServiceLocator()->get('daux.service');

        $daux->build();

        return "All docs generated successfully.";
    }

    public function setupAction(){

        /** @var DauxService $daux */
        $daux = $this->getServiceLocator()->get('daux.service');

        $options = $daux->getConfig();

        $tmpPath = getcwd() . DIRECTORY_SEPARATOR . $options->getDauxTmpPath();
        $outPath = getcwd() . DIRECTORY_SEPARATOR . $options->getGlobalOutput();

        if(!is_dir($tmpPath)){
            mkdir($tmpPath);
            echo 'DauxModule temp path created: '.$tmpPath . PHP_EOL;
        }

        if(!is_dir($outPath)){
            mkdir($outPath);
            echo 'DauxModule docs path created: '.$outPath . PHP_EOL;
        }

    }

}

